#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 50 // 50 characters + null terminator
#define DATE_LENGTH 12     // 10 characters (YYYY-MM-DD) + null terminator
#define REGISTRATION_LENGTH 6 // 6 characters + null terminator
#define MAX_STUDENTS 100 // maximum number of students in an array

struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[DATE_LENGTH];
    char registration[REGISTRATION_LENGTH];
    char program_code[5];
    float tuition;

};


void createStudent(struct Student *students, int *numStudents) {
    if (*numStudents >= MAX_STUDENTS) {
        printf("Maximum number of students reached!\n");
        return;
    }

    struct Student newStudent;

    printf("Enter student name (no spaces) :  ");
    scanf("%20s", newStudent.name);
    printf("Enter date of birth (YYYY-MM-DD):  ");
    scanf("%12s", newStudent.dob);

    printf("Enter registration number :  ");
    scanf("%6s", newStudent.registration);

    printf("Enter program code :  ");
    scanf("%4s", newStudent.program_code);

    printf("Enter annual tuition:  ");
    scanf("%f", &newStudent.tuition);

    students[*numStudents] = newStudent;
    (*numStudents)++;
}

void readStudent(struct Student *students, int numStudents) {

    if (numStudents == 0) {
        printf("No students to display.\n");
        return;
    }

    printf("Student Details:\n");
    for (int i = 0; i < numStudents; i++) {
        printf("Student %d:\n", i+1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dob);
        printf("Registration Number: %s\n", students[i].registration);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: %.2f\n", students[i].tuition);
        printf("\n");
    }
}






void updateStudent(struct Student *students, int numStudents) {

    if (numStudents == 0) {
        printf("No students to update.\n");
        return;
    }

    int studentIndex;
    printf("Enter the index of the student you want to update (1 to %d): ", numStudents);
    scanf("%d", &studentIndex);

    if (studentIndex < 1 || studentIndex > numStudents) {
        printf("Invalid student index.\n");
        return;
    }

    struct Student *studentToUpdate = &students[studentIndex - 1];

    printf("Update student details:\n");
    printf("Enter student name (up to 50 characters): ");
    scanf("%50s", studentToUpdate->name);

    printf("Enter date of birth (YYYY-MM-DD): ");
    scanf("%10s", studentToUpdate->dob);

    printf("Enter registration number (6 digits padded with zeros): ");
    scanf("%6s", studentToUpdate->registration);

    printf("Enter program code (up to 4 characters): ");
    scanf("%4s", studentToUpdate->program_code);

    printf("Enter annual tuition: ");
    scanf("%f", &studentToUpdate->tuition);

    printf("Student details updated successfully.\n");

}
